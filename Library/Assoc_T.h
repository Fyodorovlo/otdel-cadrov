#ifndef _ASSOC_T_H_
#define _ASSOC_T_H_
#include <stdio.h>
//template for struct Pair
template<class Key, class Value>
struct Pair {
	Key first;
	Value second;
	Pair() :
			first(Key()), second(NULL) {
	}
	Pair(const Key &name) :
			first(name) {
	}
};

//template for class Assoc
template<class Key, class Value>
class AssocIterator;

template<class Key, class Value>
class AssocIteratorConst;

template<class Key, class Value>
class Assoc {
	friend class AssocIterator<Key, Value> ;
	friend class AssocIteratorConst<Key, Value> ;
private:
	static const int QUOTA = 10;
	int cnt, cur;
	Pair<Key, Value> *arr;
	int getPos(const Key &) const;
public:
	Assoc() :
			cnt(QUOTA), cur(0), arr(new Pair<Key, Value> [QUOTA]) {
	}
	Assoc(const Assoc<Key, Value> &);
	~Assoc() {
		delete[] arr;
	}
	Assoc<Key, Value>& operator =(const Assoc<Key, Value> &);
	Value & operator [](const Key &);
	void insert(const Pair<Key, Value>&);
	void clear();
	Assoc<Key, Value>& qSort(int left, int right);
	Assoc<Key, Value>& sort() {
		qSort(0, cur - 1);
		return *this;
	}
	typedef AssocIterator<Key, Value> Iterator;
	Iterator begin();
	Iterator end();
	Iterator find(const Key &) const;
	int erase(const Key &);
	typedef AssocIteratorConst<Key, Value> const_Iterator;
	const_Iterator begin() const;
	const_Iterator end() const;
};

template<class Key, class Value>
Assoc<Key, Value>::Assoc(const Assoc<Key, Value> &a) :
		cnt(a.cnt), cur(a.cur), arr(new Pair<Key, Value> [a.cnt]) {
	for (int i = 0; i < cnt; i++) {
		arr[i].first = a.arr[i].first;
		arr[i].second = a.arr[i].second->clone();
	}
}

template<class Key, class Value>
Assoc<Key, Value>& Assoc<Key, Value>::operator =(const Assoc<Key, Value> &a) {
	if (this != &a) {
		delete[] arr;
		arr = new Pair<Key, Value> [cnt = a.cnt];
		cur = a.cur;
		for (int i = 0; i < cnt; i++) {
			arr[i].first = a.arr[i].first;
			arr[i].second = a.arr[i].second->clone();
		}
	}
	return *this;
}

template<class Key, class Value>
int Assoc<Key, Value>::getPos(const Key &s) const {
	for (int i = 0; i < cur; ++i)
		if (arr[i].first == s)
			return i;
	return -1;
}

template<class Key, class Value>
Value & Assoc<Key, Value>::operator [](const Key &s) {
	int i = getPos(s);
	if (i < 0) {
		i = cur;
		if (cur >= cnt) {
			Pair<Key, Value> *old = arr;
			arr = new Pair<Key, Value> [cnt += QUOTA];
			for (i = 0; i < cur; ++i)
				arr[i] = old[i];
			delete[] old;
		}
		arr[cur].first = s;
		++cur;
	}
	return arr[i].second;
}

template<class Key, class Value>
Assoc<Key, Value>& Assoc<Key, Value>::qSort(int left, int right) {
	int i = left, j = right;
	if (i >= j)
		return (*this);
	Key tmp;
	Value ptr;
	int middle = (left + right) / 2;
	while (i <= j) {
		while (arr[i].first < arr[middle].first)
			i++;
		while (arr[j].first > arr[middle].first)
			j--;
		if (i <= j) {
			tmp = arr[i].first;
			arr[i].first = arr[j].first;
			arr[j].first = tmp;
			ptr = arr[i].second;
			arr[i].second = arr[j].second;
			arr[j].second = ptr;
			i++;
			j--;

		}
	};
	if (left < j)
		qSort(left, j);
	if (i < right)
		qSort(i, right);
	return (*this);
}

template<class Key, class Value>
void Assoc<Key, Value>::insert(const Pair<Key, Value> &p) {
	int i = getPos(p.first);
	if (i < 0) {
		i = cur;
		if (cur >= cnt) {
			Pair<Key, Value> *old = arr;
			arr = new Pair<Key, Value> [cnt += QUOTA];
			for (i = 0; i < cur; ++i)
				arr[i] = old[i];
			delete[] old;
		}
		arr[cur].first = p.first;
		arr[cur].second = p.second;
		++cur;
		sort();
	}
	return;
}

template<class Key, class Value>
void Assoc<Key, Value>::clear() {
	for (int i = 0; i < cur; ++i) {
		arr[i].second.~Value();
	}
	delete[] arr;
	cur = 0;
	cnt = QUOTA;
	arr = new Pair<Key, Value> [cnt];
	return;
}

template<class Key, class Value>
AssocIterator<Key, Value> Assoc<Key, Value>::begin() {
	return AssocIterator<Key, Value>(this->arr);
}

template<class Key, class Value>
AssocIterator<Key, Value> Assoc<Key, Value>::end() {
	return AssocIterator<Key, Value>(this->arr + cur);
}

template<class Key, class Value>
AssocIterator<Key, Value> Assoc<Key, Value>::find(const Key &s) const {
	int i = getPos(s);
	if (i < 0)
		i = cur;
	return AssocIterator<Key, Value>(this->arr + i);
}

template<class Key, class Value>
int Assoc<Key, Value>::erase(const Key &s) {
	int i = getPos(s);
	int j;
	if (i < 0)
		return 0;
	//arr[i].first.~Key();
	//arr[i].second.~Value();
	int m;
	for (j = i; j < cur; j++) 
	{
		m = j + 1;
		arr[j] = arr[m];
	}
	//arr[i] = arr[--cur];
	cur--;
	return 1;
}

template<class Key, class Value>
AssocIteratorConst<Key, Value> Assoc<Key, Value>::begin() const {
	return AssocIteratorConst<Key, Value>(this->arr);
}

template<class Key, class Value>
AssocIteratorConst<Key, Value> Assoc<Key, Value>::end() const {
	return AssocIteratorConst<Key, Value>(this->arr + cur);
}

// template for class AssocIterator
template<class Key, class Value>
class AssocIterator {
private:
	Pair<Key, Value> *cur;
public:
	AssocIterator() :
			cur(0) {
	}
	AssocIterator(Pair<Key, Value> *a) :
			cur(a) {
	}
	int operator !=(const AssocIterator<Key, Value> &) const;
	int operator ==(const AssocIterator<Key, Value> &) const;
	Pair<Key, Value>& operator *();
	AssocIterator<Key, Value>& operator ++();
	AssocIterator<Key, Value> operator ++(int);
};

template<class Key, class Value>
int AssocIterator<Key, Value>::operator !=(
		const AssocIterator<Key, Value> &it) const {
	return cur != it.cur;
}

template<class Key, class Value>
int AssocIterator<Key, Value>::operator ==(
		const AssocIterator<Key, Value> &it) const {
	return cur == it.cur;
}

template<class Key, class Value>
Pair<Key, Value>& AssocIterator<Key, Value>::operator *() {
	return *cur;
}

template<class Key, class Value>
AssocIterator<Key, Value>& AssocIterator<Key, Value>::operator ++() {
	++cur;
	return *this;
}

template<class Key, class Value>
AssocIterator<Key, Value> AssocIterator<Key, Value>::operator ++(int) {
	AssocIterator<Key, Value> res(*this);
	++cur;
	return res;
}

//template for class AssocIteratorConst
template<class Key, class Value>
class AssocIteratorConst {
private:
	const Pair<Key, Value> *cur;
public:
	AssocIteratorConst() :
			cur(0) {
	}
	AssocIteratorConst(const Pair<Key, Value> *a) :
			cur(a) {
	}
	int operator !=(const AssocIteratorConst<Key, Value> &) const;
	int operator ==(const AssocIteratorConst<Key, Value> &) const;
	const Pair<Key, Value>& operator *();
	AssocIteratorConst<Key, Value>& operator ++();
	AssocIteratorConst<Key, Value> operator ++(int);
};

template<class Key, class Value>
int AssocIteratorConst<Key, Value>::operator !=(
		const AssocIteratorConst<Key, Value> &it) const {
	return cur != it.cur;
}

template<class Key, class Value>
int AssocIteratorConst<Key, Value>::operator ==(
		const AssocIteratorConst<Key, Value> &it) const {
	return cur == it.cur;
}

template<class Key, class Value>
const Pair<Key, Value>& AssocIteratorConst<Key, Value>::operator *() {
	return *cur;
}

template<class Key, class Value>
AssocIteratorConst<Key, Value>& AssocIteratorConst<Key, Value>::operator ++() {
	++cur;
	return *this;
}

template<class Key, class Value>
AssocIteratorConst<Key, Value> AssocIteratorConst<Key, Value>::operator ++(
		int) {
	AssocIteratorConst<Key, Value> res(*this);
	++cur;
	return res;
}
#endif
