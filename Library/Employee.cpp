#include "Employee.h"
#include "utility.h"
ostream& operator<<(ostream& s, const Employee& f) {
	return f.show(s);
}
istream &operator>>(istream& in, Employee& f) {
	return f.get(in);
}

fstream & Employee::read(fstream &f) {
	readString(f, name);
	f.read((char *) &dOb, sizeof(dOb));
	readString(f, post);
	readString(f, education);
	f.read((char *) &salary, sizeof(salary));
	return f;
}

fstream & Employee::write(fstream &f) {
	writeString(f, name);
	f.write((char *) &dOb, sizeof(dOb));
	writeString(f, post);
	writeString(f, education);
	f.write((char *) &salary, sizeof(salary));
	return f;
}

