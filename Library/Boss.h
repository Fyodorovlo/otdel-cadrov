#ifndef _BOSS_H_
#define _BOSS_H_

#include "Company.h"

class Boss: public Employee {
public:
	Boss(double money = 0.0) :
			Employee(money), c(NULL) {
	}
	~Boss() {
		c = NULL; //now this boss does not control any company
	}
	virtual Boss* clone() const {
		return new Boss(*this);
	}
	const string type() {
		return "Boss";
	}
	Company * getCompany() {
		return c;
	}
	Boss& setCompany(Company* comp) {
		c = comp;
		return *this;
	}

protected:
	ostream& show(ostream&) const;
	istream& get(istream&);
	Company *c;
};

#endif

