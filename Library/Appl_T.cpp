#include <fstream>
#include "Appl_T.h"
#include <iostream>
#include <string>
using namespace std;
const char *Menu[] = { "1. Add a company", "2. Find a company", "3. Show all",
		"4. Add an employee", "5. Find an employee", "6. Delete employee",
		"7. Set post", "8. Set salary", "9. Conversion of employee", "10. Save",
		"11. Load", "12. Reduction", "0. Quit" }, *Choice = "Make your choice",
		*Msg = "You are wrong; repeat please";

const int Num = sizeof(Menu) / sizeof(Menu[0]);

Appl::Appl() {
	fptr[0] = NULL;
	fptr[1] = new CAdd;
	fptr[2] = new CFind;
	fptr[3] = new CShowAll;
	fptr[4] = new CAddEmp;
	fptr[5] = new CFindEmp;
	fptr[6] = new CDelEmp;
	fptr[7] = new CSetPost;
	fptr[8] = new CSetSal;
	fptr[9] = new CModern;
	fptr[10] = new CSave;
	fptr[11] = new CLoad;
	fptr[12] = new CReduction;
}

Appl::~Appl() {
	for (int i = 0; i < 13; ++i)
		delete fptr[i];
}

int Appl::run() {
	int ind;
	while (ind = Answer(Menu, Num)) {
		(*fptr[ind])(*this);
	}
	cout << "That's all. Bye!" << endl;
	return 0;
}

int Appl::Answer(const char *alt[], int n) {
	int answer;
	const char *prompt = Choice;
	cout << "What do you want to do:" << endl;
	for (int i = 0; i < n; i++)
		cout << alt[i] << endl;
	do {
		cout << prompt << ": -> ";
		prompt = Msg;
		cin >> answer;
	} while (answer < 0 || answer >= n);
	cin.ignore(80, '\n');
	return answer;
}

int Appl::Add() {
	cout << "Enter nameCompany" << endl;
	string nameCompany;
	cin >> nameCompany;
	if (c.companyPresence(nameCompany)) {
		cout << "We already have a company with such name!\n";
		return 0;
	}
	c.companyAdd(nameCompany);
	return 1;
}

int Appl::Find() {
	cout << "Enter nameCompany (for find)" << endl;
	string nameCompany;
	cin >> nameCompany;
	if (c.companyPresence(nameCompany)) {
		cout << nameCompany << "'s employees is\n" << c.companyFind(nameCompany)
				<< endl;
		return 1;
	}
	cout << nameCompany << " not found" << endl;
	return 0;
}

int Appl::ShowAll() {
	c.sort();
	cout << c << endl;
	return 0;
}

int Appl::AddEmp() {
	cout << "Enter nameCompany (for find)" << endl;
	string nameCompany;
	cin >> nameCompany;
	if (c.companyPresence(nameCompany)) {
		c.employeeAdd(nameCompany);
		return 1;
	}
	cout << nameCompany << " not found" << endl;
	return 0;
}

int Appl::FindEmp() {
	cout << "Enter nameCompany (for find)" << endl;
	string nameCompany;
	cin >> nameCompany;
	if (c.companyPresence(nameCompany)) {
		unsigned code;
		cout << "Enter code: ";
		cin >> code;
		if (c.employeeFind(nameCompany, code) == 0) {
			cout << "Employee with code " << code << " not found!\n";
			return 0;
		} else
			cout << "For company " << nameCompany << " found:\n" << code
					<< " Employee: " << *(c.employeeFind(nameCompany, code));
		return 1;
	}
	cout << nameCompany << " not found" << endl;
	return 0;
}

int Appl::DelEmp() {
	cout << "Enter nameCompany (for delete employee)" << endl;
	string nameCompany;
	cin >> nameCompany;
	if (c.companyPresence(nameCompany)) {
		unsigned code;
		cout << "Enter code:";
		cin >> code;
		c.employeeDel(nameCompany, code);
		return 1;
	}
	cout << nameCompany << " not found" << endl;
	return 0;
}

int Appl::SetPost() {
	cout << "Enter nameCompany (for find)" << endl;
	string nameCompany;
	cin >> nameCompany;
	if (c.companyPresence(nameCompany)) { //if found company
		unsigned code;
		cout << "Enter code: ";
		cin >> code;
		if (c.employeeFind(nameCompany, code) == 0) {
			cout << "Employee with code " << code << " not found!\n";
			return 0;
		} else {
			cout << "Enter new post "; //if found code
			string newPost;
			cin >> newPost;
			c.employeeFind(nameCompany, code)->setPost(newPost); //set Post
			cout << "Post set\n";
			return 1;
		}
	}
	cout << nameCompany << " not found" << endl;
	return 0;
}

int Appl::SetSal() {
	cout << "Enter nameCompany (for find)" << endl;
	string nameCompany;
	cin >> nameCompany;
	if (c.companyPresence(nameCompany)) { //if found company
		unsigned code;
		cout << "Enter code: ";
		cin >> code;
		if (c.employeeFind(nameCompany, code) == 0) {
			cout << "Employee with code " << code << " not found!\n";
			return 0;
		} else {
			cout << "Enter new salary: "; //if found code
			double money;
			cin >> money;
			if (money <= 0) {
				cout << "Uncorrected data!" << endl;
				return -1;
			}
			c.employeeFind(nameCompany, code)->setSalary(money); //set salary
			cout << "Salary set\n";
			return 1;
		}
	}
	cout << nameCompany << " not found" << endl;
	return 0;
}

int Appl::Modern() {
	cout << "Enter nameCompany (for find)" << endl;
	string nameCompany;
	cin >> nameCompany;
	
	if (c.companyPresence(nameCompany)) {
		cout << "Enter code user, which will be increase\n";
		unsigned num;
		cin >> num;
		Company *ptr = &c;// efewhfhewufhwegh
		c.employeeModern(nameCompany, num, ptr);  /// dkjgijerihreubeh

		return 1;
	}
	cout << nameCompany << " not found" << endl;
	return 0;
}

int Appl::Reduction() {
	cout << "Enter nameCompany (for find)" << endl;
	string nameCompany;
	cin >> nameCompany;
	if (c.companyPresence(nameCompany)) {
		cout << "Enter code boss, which will be decrease\n";
		unsigned num;
		cin >> num;
		c.employeeReduction(nameCompany, num);
		return 1;
	}
	cout << nameCompany << " not found" << endl;
	return 0;
}

int Appl::Save() {
	cout << "Save data from application into file \"" << fname << '"' << endl;
	cout << "Do you want to use new file (y/n)? --> ";
	string ans;
	cin >> ans;
	if (ans == "y") {
		cout << "Enter new file name: --> ";
		cin >> fname;
	}

	fstream f(fname.c_str(), ios::out | ios::binary);
	if (!f) {
		cout << "Cannot create file " << fname << endl;
		return 1;
	}
	c.write(f);
	return 0;
}

int Appl::Load() {
	cout << "Load data from file into application" << endl;
	cout << "Enter file name: --> ";
	cin >> fname;
	fstream f(fname.c_str(), ios::in | ios::binary);
	if (!f) {
		cout << "File " << fname << " doesn't exist" << endl;
		return 1;
	}
	try {
		c.read(f);
	} catch (const char* em) {
		cout << em << endl;
	}
	cout << "Finished\n";
	return 0;
}

