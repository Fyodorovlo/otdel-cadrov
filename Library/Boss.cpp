#include "Boss.h"
#include <iostream>
#include <string>
using namespace std;
ostream& Boss::show(ostream& s) const {
	return s << "Boss company " << c->getCompanyName() << ".Name is " << name
			<< " Date of birthday: " << dOb << " Post: " << post
			<< " Education: " << education << " Salary: " << salary << endl;
}
istream& Boss::get(istream& in) {
	cout << "Enter name\n";
	in >> name;
	cout << "Enter date of birthday\n";
	in >> dOb;
	cout << "Enter post\n";
	in >> post;
	cout << "Enter education\n";
	in >> education;
	cout << "Enter salary\n";
	in >> salary;
	return in;
}

