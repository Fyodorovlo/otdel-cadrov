#ifndef _EMPLOYEE_H_
#define _EMPLOYEE_H_
#include <iostream>
using namespace std;

class Employee {
	friend ostream &operator<<(ostream&, const Employee& f);
	friend istream &operator>>(istream&, Employee& f);
protected:
	virtual ostream& show(ostream&) const=0;
	virtual istream& get(istream&)=0;
public:
	virtual fstream & read(fstream &);
	virtual fstream & write(fstream &);
	virtual ~Employee() {
	}
	virtual Employee* clone() const = 0;
	Employee(double money = 0.0) :
			salary(money) {
	}
	virtual const string type()=0;
	double getSalary() {
		return salary;
	}
	Employee& setSalary(double money) {
		salary = money;
		return *this;
	}
	string getPost() {
		return post;
	}
	Employee& setPost(const string& status) {
		post = status;
		return *this;
	}
	string getEducation() {
		return education;
	}
	Employee& setEducation(const string& newEducation) {
		education = newEducation;
		return *this;
	}
	string getName() {
		return name;
	}
	Employee& setName(const string& newName) {
		name = newName;
		return *this;
	}
	int getDateOfBirthday() {
		return dOb;
	}
	Employee& setDateOfBirthday(int date) {
		dOb = date;
		return *this;
	}
protected:
	string name;
	int dOb; //date of birthday
	string post; //status
	string education;
	double salary;
};

#endif

