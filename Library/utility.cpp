#include "utility.h"

fstream & writeString(fstream &f, const string &out) {
	int l = out.size();
	const char * str = out.c_str();
	f.write((char*) &l, sizeof(l));
	f.write(str, l); //write without '\0'
	return f;
}

fstream & readString(fstream &f, string &in) {
	int l;
	f.read((char*) &l, sizeof(l));
	if (f.eof())
		return f;
	char * str = new char[l + 1];
	f.read(str, l);
	str[l] = '\0'; //add '\0'
	in = str;
	delete[] str;
	return f;
}

