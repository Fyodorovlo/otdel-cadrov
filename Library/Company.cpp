#include "Company.h"
#include "Boss.h"
#include "User.h"
#include <fstream>
#include "utility.h"
//#include "iostream"
using namespace std;
int Company::companyAddWithBoss(unsigned codeBoss, Employee* n, string& oldnameC, unsigned* oldcodeBoss) {
	//string& soncompboss
	Company* comp = NULL;
	string nameC;
	cout << "Enter Company name\n";
	cin >> nameC;
	//soncompboss = nameC;
	if (nameC == oldnameC) { 
		cout << "Please use Reduction function to change Boss for this company\n";
		Assoc<string, Company*>::Iterator ifind = department.find(oldnameC);
		(*ifind).second->tableEmployers.deleteEmployee(codeBoss);
		//(*ifind).second->tableEmployers.sort();
		return -1;
	}
	Assoc<string, Company*>::Iterator p1 = department.find(nameC);
	if (p1 != department.end())   //�������� ��� ��������� 
	 *oldcodeBoss = (*p1).second->tableEmployers.findBossCode(nameC); //find boss company
	
	 Assoc<string, Company*>::Iterator p = department.find(nameC);
	if (p != department.end())
		delete (*p).second; 
	comp = new Company(nameC);
	Boss* b = dynamic_cast<Boss *>(n);
	b->setCompany(comp);
	comp->tableEmployers.copyBoss(codeBoss, n);
	department[nameC] = comp;
	sort();
	return 0;
}

Employee* Company::employeeFind(string& nameC, unsigned code) {
	Assoc<string, Company*>::Iterator p = department.find(nameC);
	return (*p).second->tableEmployers.findEmployee(code);
}

int Company::companyAdd(string& nameC) {
	Company* comp = NULL;
	int codeBoss;
	comp = new Company(nameC);
	Employee* employeeCompany = comp->tableEmployers.addBossCompany();
	Boss* bossCompany = dynamic_cast<Boss *>(employeeCompany);
	bossCompany->setCompany(comp);
//	if ((codeBoss = comp->tableEmployers.addEmployee()) != -1) { //if boss added add new company
//		department[nameC] = comp;
//		Employee* ptr3 = new Boss();
//		Employee* n = comp->tableEmployers.findEmployee(codeBoss);
//		(*ptr3) = (*n);
////		companyAddWithBoss(codeBoss, n);
//		companyAddWithBoss(codeBoss, ptr3);
//		return 1;
//	} else
		department[nameC] = comp;
	sort();
	return 0;
}

TableEmp& Company::companyFind(string& nameC) {
	Assoc<string, Company*>::Iterator ifind = department.find(nameC);
	return (*ifind).second->tableEmployers;
}

int Company::companyPresence(string& nameC) {
	Assoc<string, Company*>::Iterator ifind = department.find(nameC);
	if (ifind != department.end())
		return 1;
	else
		return 0;
}

void Company::globalChangeBoss(unsigned codeOldBoss, Employee* oldEmployee,
		unsigned codeNewBoss, Employee* newEmployee) {
	for (Assoc<string, Company*>::Iterator p = department.begin();
			p != department.end(); ++p) {
		Employee* suspectEmployee = (*p).second->tableEmployers.findEmployee(
				codeOldBoss); //find old boss
		if ((suspectEmployee != 0) && (suspectEmployee->type() == "Boss")) {
			Boss* oldB = dynamic_cast<Boss *>(oldEmployee);
			oldB->setCompany(NULL);
			(*p).second->tableEmployers.deleteEmployee(codeOldBoss); //delete him
			(*p).second->tableEmployers.copyBoss(codeNewBoss, newEmployee); //change boss
		}
	}
	for (Assoc<string, Company*>::Iterator p = department.begin();
			p != department.end(); ++p)
		(*p).second->tableEmployers.sort();
}

int Company::employeeModern(string& nameC, unsigned codeNewBoss,Company *c) {   /// bpvtybj kjfiejih
	Assoc<string, Company*>::Iterator ifind = department.find(nameC);
	Employee* increaseEmployee = (*ifind).second->tableEmployers.findEmployee(
			codeNewBoss);
	if ((increaseEmployee == 0) || (increaseEmployee->type() == "Boss")) {
		cout << "User with code " << codeNewBoss << " not found" << endl;
		return 1;
	}
	unsigned codeOldBoss = (*ifind).second->tableEmployers.findBossCode(nameC); //find boss company
	//with help name company
	
	Employee* oldEmployee = (*ifind).second->tableEmployers.findBoss(nameC); //find old boss
	//cout << "Old boss info: " << &oldEmployee << endl;
	//////////////////////////////////////////////////////////////////////
	/*if (&*c->employeeFind(nameCompany, codeOldBoss) == 0) {
		cout << "Employee with code " << codeOldBoss << " not found!\n";
		return 0;
	}
	else
		cout << "For company " << nameCompany << " found:\n" << codeOldBoss
		<< " Employee: " << *(c->employeeFind(nameCompany, codeOldBoss));
	return 1;*/
	//////////////////////////////////////////////////////////
	Boss* oldBoss = dynamic_cast<Boss *>(oldEmployee);
	//cout << "Old boss of "<<nameC <<"\n" <<"info: "<<"\n"<< "Code: " << codeOldBoss<< endl;
	//cout << "Old boss info: " << &oldEmployee << endl;
	//////////////////////////
	//throw
	//Boss* oldBoss2 = dynamic_cast<Boss *>(oldEmployee);


	cout << "Ex-boss of " << nameC << " info: " << "\n" <<" Code: " << codeOldBoss << "\n" <<" Name: " << oldEmployee->getName() << "\n" <<" Post: " << oldEmployee->getPost() << "\n"<<" DateOfBirthday: " << oldEmployee->getDateOfBirthday() << "\n"<< " Education: " << oldEmployee->getEducation() << "\n" <<" Salary: " << oldEmployee->getSalary() << endl;
	//////////////////////////////


	Employee* newEmployee = (*ifind).second->tableEmployers.copyDataUserToBoss(
			codeNewBoss, increaseEmployee); //add new Boss
	Boss* newBoss = dynamic_cast<Boss *>(newEmployee);
	newBoss->setCompany(oldBoss->getCompany());
	oldBoss->setCompany(NULL);
	(*ifind).second->tableEmployers.deleteEmployee(codeOldBoss);

	//Global change Boss code
	globalChangeBoss(codeOldBoss, oldEmployee, codeNewBoss, newEmployee);
	return 0;
}

void Company::globalDeleteBoss(unsigned code, Employee* removeEmployee) {  //�������� ����� � ���������� �������� "c"
	for (Assoc<string, Company*>::Iterator p = department.begin();
			p != department.end(); ++p) {
		Employee* suspectForRemoveEmployee =
				(*p).second->tableEmployers.findEmployee(code);
		if ((suspectForRemoveEmployee != 0)
				&& (suspectForRemoveEmployee->type() == "Boss")) {
			Boss* boss = dynamic_cast<Boss *>(removeEmployee);
			boss->setCompany(NULL);
			(*p).second->tableEmployers.deleteEmployee(code);
		}
	}
}

int Company::employeeReduction(string& nameC, unsigned codeBoss) {
	Assoc<string, Company*>::Iterator ifind = department.find(nameC);
	Employee* decreaseEmployee = (*ifind).second->tableEmployers.findEmployee(
			codeBoss);
	if ((decreaseEmployee == 0) || (decreaseEmployee->type() == "User")) {
		cout << "Boss with code " << codeBoss << " not found" << endl;
		return 1;
	}
	Employee* ptr = NULL;
	ptr = new User();
	ptr->setPost(decreaseEmployee->getPost());
	ptr->setDateOfBirthday(decreaseEmployee->getDateOfBirthday());
	ptr->setName(decreaseEmployee->getName());
	ptr->setEducation(decreaseEmployee->getEducation());
	ptr->setSalary(decreaseEmployee->getSalary());
	employeeDel(nameC, codeBoss);
	(*ifind).second->tableEmployers.insertReductionBoss(codeBoss, ptr);
	return 0;
}
int Company::employeeDel(string& nameC, unsigned code) {  // �������� � ����������� 
	Assoc<string, Company*>::Iterator ifind = department.find(nameC);
	Employee* removeEmployee = (*ifind).second->tableEmployers.findEmployee(
			code);
	if (removeEmployee == 0) {
		cout << "There no employee with code " << code << endl;
		return -1;
	}
	if (removeEmployee->type() == "User") {
		(*ifind).second->tableEmployers.deleteEmployee(code);
		cout << "Deleted\n";
	} else {
		Boss* removeBoss = dynamic_cast<Boss *>(removeEmployee);
		Employee* newEmpoyeeCompany =
				removeBoss->getCompany()->tableEmployers.addBossCompany();
		Boss* newBossCompany = dynamic_cast<Boss *>(newEmpoyeeCompany);
		newBossCompany->setCompany(removeBoss->getCompany());
		removeBoss->setCompany(NULL);
		(*ifind).second->tableEmployers.deleteEmployee(code);
		//Global delete Boss code
		globalDeleteBoss(code, removeEmployee);
	}
	return 0;
}

int Company::employeeAdd(string& nameC) {
	int codeBoss;
	unsigned oldcodeBoss=0;
	Employee* removeEmployee=new Boss();
	/*string newNameC;
	cout << "Enter new Company name\n";
	cin >> newNameC;*/
	/*if (nameC == newNameC) { cout << "Please use Reduction function to change Boss for this company\n"; return -1; }*/
	Assoc<string, Company*>::Iterator ifind = department.find(nameC);

	if ((codeBoss = (*ifind).second->tableEmployers.addEmployee()) != -1) {
		Employee* n = (*ifind).second->tableEmployers.findEmployee(codeBoss);
		companyAddWithBoss(codeBoss, n, nameC, &oldcodeBoss);
		/*Assoc<string, Company*>::Iterator ifind = department.find(nameC);
		Employee* removeEmployee = (*ifind).second->tableEmployers.findEmployee(
		code);*/
		if (oldcodeBoss!=0 ) globalDeleteBoss(oldcodeBoss, removeEmployee);
		
		//Assoc<string, Company*>::Iterator p = department.find(soncompboss);
		//unsigned oldcodeBoss = (*p).second->tableEmployers.findBossCode(soncompboss); //find boss company
		//if (oldcodeBoss !=0)																			//with help name company
		//oldcodeBoss = findBossCode(soncompboss);
		//employeeDel(nameC, oldcodeBoss);
		return 1;
	}
	return 0;
}

ostream &operator<<(ostream& out, const Company& c) {
	Assoc<string, Company*>::const_Iterator i;
	for (i = c.department.begin(); i != c.department.end(); ++i) {
		out << "Company: " << (*i).first << "'s employees is\n"
				<< (*i).second->tableEmployers << endl;
	}
	return out;
}

fstream & fwrite(fstream &f, const Pair<string, Company *> &p) {
	writeString(f, p.first);
	p.second->getTableEmpoyeers().writeTable(f);
	return f;
}

fstream & Company::loadCompanies(fstream &f, int count) {

	for (; count > 0; --count) {
		Pair<string, Company *> a;
		readString(f, a.first);
		a.second = NULL;
		a.second = new Company(a.first);
		department.insert(a);
	}
	return f;
}

fstream & Company::read(fstream &f) {
	department.clear();
	int countCompany;
	f.read((char *) &countCompany, sizeof(countCompany));
	loadCompanies(f, countCompany); ////read name companies and create
	//write table and record
	Assoc<string, Company*>::Iterator ifind;
	int countEmployee;
	for (; countCompany > 0; --countCompany) { //find companies and write information
		string nameC;
		readString(f, nameC);
		ifind = department.find(nameC);

		if (ifind == department.end())
			throw "Error with read(company not found)";
		f.read((char *) &countEmployee, sizeof(countEmployee));

		for (; countEmployee > 0; --countEmployee) {
			Pair<unsigned, Employee *> p;
			f.read((char *) &(p.first), sizeof((p.first))); //read code
			int typeEmployee;
			f.read((char *) &typeEmployee, sizeof(typeEmployee)); //read type
			p.second = NULL;
			if (typeEmployee == 1) {

				p.second = new Boss;
				Boss* boss = dynamic_cast<Boss *>(p.second); //if boss
				string nameC;
				readString(f, nameC);
				Assoc<string, Company*>::Iterator i = department.find(nameC);

				if (i == department.end())
					throw "Error with read(company not found)";

				boss->setCompany((*i).second); //set company

			}

			else if (typeEmployee == 0) {
				p.second = new User;

			} else
				throw "Error with read(unknown type)";
			p.second->read(f); //read information
			(*ifind).second->getTableEmpoyeers().insertPair(f, p);
		}
	}
	return f;
}

fstream & Company::write(fstream &f) const {
	int countCompany = 0;
	for (Assoc<string, Company *>::const_Iterator p = department.begin();
			p != department.end(); ++p)
		countCompany++;

	f.write((char *) &countCompany, sizeof(countCompany)); //write count companies
	for (Assoc<string, Company *>::const_Iterator p = department.begin();
			p != department.end(); ++p)
		writeString(f, (*p).first); //write name companies

	for (Assoc<string, Company *>::const_Iterator p = department.begin();
			p != department.end(); ++p)
		fwrite(f, *p); //write companies

	return f;
}

