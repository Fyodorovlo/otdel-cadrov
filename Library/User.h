#ifndef _USER_H_
#define _USER_H_
#include "Employee.h"

class User: public Employee {
public:
	User(double money = 0.0) :
			Employee(money) {
	}
	virtual User* clone() const {
		return new User(*this);
	}
	const string type() {
		return "User";
	}

protected:
	ostream& show(ostream&) const;
	istream& get(istream&);
};
#endif
