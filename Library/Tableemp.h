#ifndef _TABLEEMP_H_
#define _TABLEEMP_H_
#include "Assoc_T.h"
#include "Employee.h"

class TableEmp {
	friend ostream &operator<<(ostream&, const TableEmp& t);
public:
	TableEmp() {
	}
	~TableEmp() {
		for (Assoc<unsigned, Employee*>::Iterator p = book.begin();
				p != book.end(); ++p) {
			delete (*p).second;
		}
	}
	fstream & insertPair(fstream &f, Pair<unsigned, Employee *> &p);
	fstream & writeTable(fstream &f) const;
	int addEmployee();
	Employee* copyDataUserToBoss(unsigned, Employee*);
	void insertReductionBoss(unsigned, Employee*);
	Employee* findBoss(string&);
	unsigned findBossCode(string&);
	int deleteEmployee(unsigned);
	int copyBoss(unsigned, Employee*);
	Employee* addBossCompany();
	Employee* findEmployee(unsigned);
	void sort() {
		book.sort();
	}
protected:
	unsigned code;
	Assoc<unsigned, Employee*> book;
};

#endif
