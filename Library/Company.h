#ifndef _COMPANY_H_
#define _COMPANY_H_
#include "Tableemp.h"
#include <string.h>
using namespace std;
class Company {
	friend ostream &operator<<(ostream&, const Company& c);
public:
	Company() {
	}
	Company(string& newName) :
			nameCompany(newName) {
	}
	~Company() {
		for (Assoc<string, Company*>::Iterator p = department.begin(); p != department.end();
				++p) {
			(*p).second->tableEmployers.~TableEmp();
			delete (*p).second;
		}
	}
	string getCompanyName() {
		return nameCompany;
	}
	Company& setCompanyName(string newName) {
		nameCompany = newName;
		return *this;
	}
	int companyPresence(string&);
	int companyAdd(string&);
	int employeeAdd(string&);
	TableEmp& companyFind(string&);
	Employee* employeeFind(string&, unsigned);
	int employeeDel(string&, unsigned);
	int employeeModern(string&, unsigned,Company* c); /// bpvtybkfjke
	int employeeReduction(string&, unsigned);
	TableEmp& getTableEmpoyeers() {
		return tableEmployers;
	}
	void sort() {
		department.sort();
	}
	fstream &read(fstream &);
	fstream &write(fstream &) const;
protected:
	fstream &loadCompanies(fstream &, int);
	void globalChangeBoss(unsigned codeOld, Employee* oldEmp, unsigned codeNew,
			Employee* newEmp);
	void globalDeleteBoss(unsigned, Employee*);
	int companyAddWithBoss(unsigned, Employee*, string&, unsigned*);
	TableEmp tableEmployers;
	string nameCompany;
	Assoc<string, Company*> department;
};
#endif
