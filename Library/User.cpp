#include "User.h"
#include <fstream>
#include <string>
using namespace std;
ostream& User::show(ostream& s) const {
	return s << "User.Name is " << name << " Date of birthday: " << dOb
			<< " Post: " << post << " Education: " << education << " Salary: "
			<< salary << endl;
}

istream& User::get(istream& in) {
	cout << "Enter name\n";
	in >> name;
	cout << "Enter date of birthday\n";
	in >> dOb;
	cout << "Enter post\n";
	in >> post;
	cout << "Enter education\n";
	in >> education;
	cout << "Enter salary\n";
	in >> salary;
	return in;
}
