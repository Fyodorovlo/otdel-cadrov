#ifndef _APPL_STL_H_
#define _APPL_STL_H_
#include "Company.h"
using namespace std;

class Appl {
private:
	class Funcs {
	public:
		virtual int operator()(Appl &a) = 0;
	};
	Company c;
	string fname;
	Funcs * fptr[13];
public:
	Appl();
	~Appl();
	int run();
	int Answer(const char *alt[], int n);
	int Add();
	int Find();
	int ShowAll();
	int AddEmp();
	int FindEmp();
	int DelEmp();
	int SetPost();
	int SetSal();
	int Modern();
	int Save();
	int Load();
	int Reduction();
private:
	class CAdd: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.Add();
		}
	};

	class CFind: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.Find();
		}
	};

	class CShowAll: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.ShowAll();
		}
	};

	class CAddEmp: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.AddEmp();
		}
	};

	class CFindEmp: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.FindEmp();
		}
	};

	class CDelEmp: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.DelEmp();
		}
	};

	class CSetPost: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.SetPost();
		}
	};

	class CSetSal: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.SetSal();
		}
	};

	class CModern: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.Modern();
		}
	};

	class CSave: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.Save();
		}
	};

	class CLoad: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.Load();
		}
	};

	class CReduction: public Funcs {
	public:
		int operator()(Appl &a) {
			return a.Reduction();
		}
	};
};

#endif
