#include "Tableemp.h"
#include "User.h"
#include "Boss.h"
#include <fstream>
#include "utility.h"
Employee* TableEmp::findEmployee(unsigned number) {
	Assoc<unsigned, Employee*>::Iterator ifind = book.find(number);
	if (ifind != book.end())
		return ((*ifind).second);
	else
		return 0;
}

int TableEmp::copyBoss(unsigned codeBoss, Employee* n) {
	book[codeBoss] = n->clone();
	return 0;
}

int TableEmp::addEmployee() {
	Employee* ptr = NULL;
	bool b = 0;
	char Type;
	do {
		cout << "	Enter S for User,\n	" << "C  Boss, " << " : ";
		cin >> Type;
		do {
			cout << "Enter code\n";
			cin >> code;
			Assoc<unsigned, Employee*>::Iterator i = book.find(code);
			if (i != book.end())
				cout << "We already have a person with such code!\n";
			else
				b = 1;
		} while (b == 0);
		switch (Type) {
		case 'c':
		case 'C':
			ptr = new Boss();
			break;
		case 's':
		case 'S':
			ptr = new User();
			break;
		default:
			cout << "Sorry, wrong choice about type.\n ";
			break;
		}
	} while (Type != 's' && Type != 'S' && Type != 'c' && Type != 'C');
	cin >> (*ptr);
	book[code] = ptr;
	sort();
	if (ptr->type() == "Boss")
		return code;
	return -1;
}

Employee* TableEmp::addBossCompany() {
	Employee* ptr = NULL;
	cout << "Enter boss of company\n";
	bool b = 0;
	do {
		cout << "Enter code\n";
		cin >> code;
		Assoc<unsigned, Employee*>::Iterator i = book.find(code);
		if (i != book.end())
			cout << "We already have a person with such code!\n";
		else
			b = 1;
	} while (b == 0);
	ptr = new Boss();
	cin >> (*ptr);
	book[code] = ptr;
	sort();
	return ptr;
}

int TableEmp::deleteEmployee(unsigned code) {
	Assoc<unsigned, Employee*>::Iterator it = book.find(code);
	if (it != book.end()) {
		book.erase(code);
		return 1;
	} else {
		cout << "User not found\n";
		return 0;
	}
}

ostream &operator<<(ostream& out, const TableEmp& t) {
	Assoc<unsigned, Employee*>::const_Iterator i;

	for (i = t.book.begin(); i != t.book.end(); ++i)
		out << (*i).first << "\t " << *((*i).second) << endl;
	return out;
}

unsigned TableEmp::findBossCode(string& nameC) {
	for (Assoc<unsigned, Employee*>::Iterator p = book.begin(); p != book.end();
			++p) {
		if ((*p).second->type() == "Boss") {
			Boss* b = dynamic_cast<Boss *>((*p).second);
			if (b->getCompany()->getCompanyName() == nameC)
				return ((*p).first);
		}
	}
	return 0;
}
Employee* TableEmp::findBoss(string& nameC) {
	for (Assoc<unsigned, Employee*>::Iterator p = book.begin(); p != book.end();
			++p) {
		if ((*p).second->type() == "Boss") {
			Boss* b = dynamic_cast<Boss *>((*p).second);
			if (b->getCompany()->getCompanyName() == nameC)
				return ((*p).second);
		}
	}
	return 0;
}

Employee* TableEmp::copyDataUserToBoss(unsigned num, Employee* e) {
	//deleteEmployee(num);
	Employee* ptr = NULL;
	ptr = new Boss();
	ptr->setPost(e->getPost());
	ptr->setDateOfBirthday(e->getDateOfBirthday());
	ptr->setName(e->getName());
	ptr->setEducation(e->getEducation());
	ptr->setSalary(e->getSalary());
	deleteEmployee(num);
	book[num] = ptr;
	return ptr;
}

void TableEmp::insertReductionBoss(unsigned num, Employee* employee) {//
	book[num] = employee;
	sort();
	return;
}

fstream & TableEmp::insertPair(fstream &f, Pair<unsigned, Employee *> &p) {
	book.insert(p);
	return f;
}

fstream & TableEmp::writeTable(fstream &f) const {
	int countEmployee = 0;
	for (Assoc<unsigned, Employee*>::const_Iterator p = book.begin();
			p != book.end(); ++p)
		countEmployee++;
	f.write((char *) &countEmployee, sizeof(countEmployee)); //write count employers
	for (Assoc<unsigned, Employee *>::const_Iterator it = book.begin();
			it != book.end(); ++it) {
		f.write((char *) &((*it).first), sizeof((*it).first)); //write code
		int typeEmployee;
		if ((*it).second->type() == "Boss") {
			typeEmployee = 1;
			f.write((char *) &typeEmployee, sizeof(typeEmployee)); //write type

			Boss* boss = dynamic_cast<Boss *>((*it).second);

			writeString(f, boss->getCompany()->getCompanyName()); //if type=="Boss" write nameCompany

		} else {
			typeEmployee = 0;

			f.write((char *) &typeEmployee, sizeof(typeEmployee)); //write type

		}
		(*it).second->write(f);
	}
	return f;
}
