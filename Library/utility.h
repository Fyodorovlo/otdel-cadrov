#include <fstream>
#include <string>
using namespace std;
#ifndef UTILITY_H_
#define UTILITY_H_
fstream & writeString(fstream &f, const string &out);
fstream & readString(fstream &f, string &in);

#endif
